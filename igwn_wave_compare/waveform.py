#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Waveforms routines.
"""

from __future__ import (division, print_function, absolute_import)

import numpy as np
from scipy import interpolate

import os
import sys

import pycbc.inject
from .strain import Strain
# from .posterior import create_xml_table_samples
import lalsimulation as ls
import lal
import logging
from pycbc.conversions import tau0_from_mass1_mass2
from pycbc.filter import resample_to_delta_t
from pycbc.waveform import get_td_waveform, get_td_waveform_from_fd, utils as wfutils
from pycbc.types import float64, float32
from pycbc.detector import Detector

from ligo.lw import utils as ligolw_utils 
from ligo.lw import lsctables, ligolw, table

from igwn_wave_compare import iwc_io as io

# Max amplitude orders found in LALSimulation (not accessible from outside of LALSim) */
MAX_NONPRECESSING_AMP_PN_ORDER = 6
MAX_PRECESSING_AMP_PN_ORDER = 3


class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
    pass 


lsctables.use_in(LIGOLWContentHandler)


def optimal_network_snr(h_list_F, psds=None):
    '''
    function to compute optimal network snr: sqrt(<h|h>). 
    
    Parameters
    ----------
    h_list_F : list[pycbc.types.FrequencySeries]
        List of PE Max Log Likelihood Waveforms to compute optimal SNRs of.
    psds: list[pycbc.types.FrequencySeries]:
        List of PSD FrequencySeries to use for matched filter weighting 

    Returns
    -------
    snr_opt: float
        optimal network matched filter snr (norm of matched filter SNR with real data)
    '''
    
    snr_opt = np.real(np.sqrt( ### sqrt to get sigma (SNR) instead of sigma^2
        np.sum( 
            [
                (4 / h_F.duration) * np.sum(np.conj(np.array(h_F)) * np.array(h_F) / np.array(psd)) for h_F, psd in zip(h_list_F, psds)
            ]
        )
    ))
    return snr_opt

### MARK DEV: GENERAL
def flow_to_fmin(flow, amp_order, approx):
    """
    Compute the minimum frequency for waveform generation using amplitude orders
    above Newtonian.  The waveform generator turns on all orders at the orbital
    associated with fMin, so information from higher orders is not included at fLow
    unless fMin is sufficiently low.

    This function replicates conservative choices made in LALInference
    """
    if amp_order == -1:
        if approx == ls.SpinTaylorT2 or approx == ls.SpinTaylorT4:
            amp_order = MAX_PRECESSING_AMP_PN_ORDER
        else:
            amp_order = MAX_NONPRECESSING_AMP_PN_ORDER
    return flow * 2./(amp_order+2)

### MARK DEV: GENERAL
def calibrate_strain(strain, spcal_logfreqs, spcal_amp, spcal_phase):
    '''
    Calibrate a strain given calibration frequencies yielding phase and amplitude correction points in Fourier space, for onsource time.
    (PyCBC implementation)
    '''
    ### if we do not have calibration params
    if len(spcal_logfreqs) == len(spcal_amp) == len(spcal_phase) == 0:
        return strain
    
    ### From pycbc
    ### fit nominal calibration amplitude and phase params to strain data (da and dphi)
    da = interpolate.interp1d(spcal_logfreqs, spcal_amp, kind='cubic', bounds_error=False, fill_value="extrapolate")(
        np.log(strain.freqs))
    dphi = interpolate.interp1d(spcal_logfreqs, spcal_phase, kind='cubic', bounds_error=False,
                                fill_value="extrapolate")(np.log(strain.freqs))

    ### create calibration correction factor - TODO: more info on this arithmetic
    cal_factor = (1.0 + da) * (2.0 + 1j * dphi) / (2.0 - 1j * dphi)
    ### calibrate strain
    strain.data *= cal_factor
    return strain

def calibrate_strains_from_samples(strains, samples, calibration_frequencies, ifos):
    '''
    Calibrate a strain given PE params yielding phase and amplitude correction points in Fourier space, for onsource time.
    '''
    sample_params = samples.dtype.names

    cal_strains = []
    for i, ifo in enumerate(ifos):
        
        # Amplitude calibration model ## if calibrated amplitude in sample params
        amp_params = sorted([param for param in sample_params if
                             ('recalib_{0}_amplitude'.format(ifo.lower()) in param or
                              'recalib_{0}_amplitude'.format(ifo) in param)])

        # Phase calibration model ## if calibrated phase in sample params
        phase_params = sorted([param for param in sample_params if
                               ('recalib_{0}_phase'.format(ifo.lower()) in param or
                                'recalib_{0}_phase'.format(ifo) in param)])

        logfreqs = np.log(calibration_frequencies[ifo])
        data = []
        ifo_strains = strains[i]
        for s, sample in enumerate(samples):
            ### get calibration parameters (amplitude and phase)
            spcal_amp = [sample[param] for param in amp_params]
            spcal_phase = [sample[param] for param in phase_params]
            ### calibrate strains for each sample from calib. params
            data.append(calibrate_strain(ifo_strains[s], logfreqs, spcal_amp, spcal_phase))
        cal_strains.append(data)

    return cal_strains

injection_func_map = {
    np.dtype(float32): ls.SimAddInjectionREAL4TimeSeries,
    np.dtype(float64): ls.SimAddInjectionREAL8TimeSeries
}

def apply_waveform_to_strain(strain, inj, detector_name, f_lower=None, distance_scale=1,
                             phase_order=-1, injection_sample_rate=None, **kwargs):

    """Add injections (as seen by a particular detector) to a time series.

    Parameters
    ----------
    strain : TimeSeries
        Time series to inject signals into, of type float32 or float64.
    inj: Injection row
        Injection row containing parameters of the required strain
    detector_name : string
        Name of the detector used for projecting injections.
    f_lower : {None, float}, optional
        Low-frequency cutoff for injected signals. If None, use value
        provided by each injection.
    distance_scale: {1, float}, optional
        Factor to scale the distance of an injection with. The default is
        no scaling.
    amp_order: {0, int} optional
        pN Amplitude order
    injection_sample_rate: float, optional
        The sample rate to generate the signal before injection
    phase_order: {0, int} optional
        pN Phase order: -1 yields highest in template
    f_ref: int optional
        reference frequency to use in generation of radiation-frame waveform created by PyCBC 
    
    **kwargs: 
        Pass optional arguments (above; and others) through make_strain_from_inj_object to get_td_waveform with dictionary comprehension.
        
    Returns
    -------
    None

    Raises
    ------
    TypeError
        For invalid types of `strain`.
    """
  
    if strain.dtype not in (float32, float64):
        raise TypeError("Strain dtype must be float32 or float64, not "
                        + str(strain.dtype))
    lalstrain = strain.lal()  
    ### Dividing the radius of the Earth by the speed of light gives the time taken for the signal to travel the distance equal to the diameter of the Earth.
    
    earth_travel_time = lal.REARTH_SI / lal.C_SI
    t0 = float(strain.start_time) - earth_travel_time
    t1 = float(strain.end_time) + earth_travel_time

    # pick lalsimulation injection function
    add_injection = injection_func_map[strain.dtype]

    delta_t = strain.delta_t
    if injection_sample_rate is not None:
        delta_t = 1.0 / injection_sample_rate

    f_l = inj.f_lower if f_lower is None else f_lower
    # roughly estimate if the injection may overlap with the segment (tau0)
    # Add 2s to end_time to account for ringdown and light-travel delay
    end_time = inj.time_geocent + 2 
    inj_length = tau0_from_mass1_mass2(inj.mass1, inj.mass2, f_l)
    
    # Start time is taken as twice approx waveform length with a 1s
    # safety buffer (either side)
    start_time = inj.time_geocent - 2 * (inj_length + 1) 
    
    ### safety check for strain duration of segment 
    if end_time < t0 or start_time > t1:
        raise ValueError("Signal lies outside the 'strain' time window")
    
    ### Make strain
    signal = make_strain_from_inj_object(inj, delta_t,
                                         detector_name, f_lower=f_l, distance_scale=distance_scale,
                                        phase_order=phase_order, **kwargs)
    ### resample correctly
    signal = resample_to_delta_t(signal, strain.delta_t, method='ldas') ### pycbc resample

    if float(signal.start_time) > t1:
        raise ValueError("Signal begins after 'strain' ends")

    ### Fine to use Lal here for just strain making
    signal = signal.astype(strain.dtype)
    signal_lal = signal.lal()
    add_injection(lalstrain, signal_lal, None)
    
    strain.data[:] = lalstrain.data.data[:]


def make_strain_from_inj_object(inj, delta_t, detector_name,
                                f_lower=None, distance_scale=1,
                                amp_order=0, phase_order=-1, **kwargs):
    """Make a h(t) strain time-series from an injection object as read from
    a sim_inspiral table, for example.

    Parameters
    -----------
    inj : injection object
        The injection object to turn into a strain h(t).
    delta_t : float
        Sample rate to make injection at.
    detector_name : string
        Name of the detector used for projecting injections.
    f_lower : {None, float}, optional
        Low-frequency cutoff for injected signals. If None, use value
        provided by each injection.
    amp_order : {0, int} optional
        pN Amplitude order
    phase_order: {0, int} optional
        pN Phase order: -1 yields highest in template
    distance_scale: {1, float}, optional
        Factor to scale the distance of an injection with. The default is
        no scaling.

    Returns
    --------
    signal : float
        h(t) corresponding to the injection.
    """
    f_l = inj.f_lower if f_lower is None else f_lower
    name = legacy_approximant_name(inj.waveform) ### 
    fmin = flow_to_fmin(f_l, amp_order, name) ### 

    # compute the waveform time series in radiation frame (for current detector)
    '''mark dev: good place to work on **kwargs all the params'''
    hp, hc = get_td_waveform(template=inj, approximant=name,
                                         delta_t=delta_t, phase_order=phase_order,
                                         f_lower=fmin, distance=inj.distance, amplitude_order=amp_order,
                                         **kwargs)
    ### project onto current detector
    return projector(detector_name,
                     inj, hp, hc, distance_scale=distance_scale)


def projector(detector_name, inj, hp, hc, distance_scale=1):
    """
    Use the injection row to project the polarizations into the
    detector frame
    """
    detector = Detector(detector_name)

    hp /= distance_scale
    hc /= distance_scale

    try:
        tc = inj.tc
        ra = inj.ra
        dec = inj.dec
    except:
        tc = inj.time_geocent

        ra = inj.longitude
        dec = inj.latitude

    hp.start_time += tc
    hc.start_time += tc

    # taper the polarizations
    ### taper horizontal and cross TS in radiation frame before projection
    try:
        hp_tapered = wfutils.taper_timeseries(hp, inj.taper)
        hc_tapered = wfutils.taper_timeseries(hc, inj.taper)
    except AttributeError:
        ### by default, in injections made for IWC, taper set to TAPER_NONE 
        hp_tapered = hp
        hc_tapered = hc

    ### taking this out unless we need it here... 
    # projection_method = 'lal'
    # if hasattr(inj, 'detector_projection_method'): ### (is not blank)
    #     projection_method = inj.detector_projection_method
    #     '''Mark Dev: SimInspiral object will probably pitch a fit when it gets here. Not a valid SimInspiral column?'''

    # logging.info('Injecting at %s, method is %s', tc, projection_method)

    # compute the detector response and add it to the strain
    signal = detector.project_wave(hp_tapered, hc_tapered,
                                   ra, dec, inj.polarization)
    return signal ### here is when we finally get signal


def legacy_approximant_name(apx):
    """Convert the old style xml approximant name to a name
    and phase_order. Alex: I hate this function. Please delete this when we
    use Collin's new tables.
    NOTE: need this for BayesWave XLAL IO, it would seem. It seems it should stay (not a matter of using udated tables)
    """
    # apx = str(apx)
    # try:
    #     order = ls.GetOrderFromString(apx)
    # except:
    #     logging.warning("Warning: Could not read phase order from string, using default")
    #     order = -1
    name = ls.GetStringFromApproximant(ls.GetApproximantFromString(apx))
    return name

def generate_strains_from_samples(samples=None, sample_rate=16384., duration=8., epoch=0., ifos=None,
                                  flow=None, approx=None, calibration_frequencies=None,
                                  fref=None, amp_order=0, phase_order=-1, xml_filename=None, sampleType='singleLogPost', **kwargs):
    """Generate calibrated strain time-series for IFOs, with injected WF from PE samples. Defaults to injecting MAP waveform from PE.  

    Parameters
    -----------
    samples : structured numpy array
        parsed PE samples
    sample_rate : int
        Sample rate to make injection at.
    duration : int
        duration of analysis segment.
    epoch : int
        starting time of segment. Defaulting to 0, to be added to a real time later if one is not passed.
    ifos : str[]
        List of ifos to make strains for
    flow : int 
        Lower frequency bound to use for making strain
    approx : str
        Name of approximant to use in making strain 
    calibration_frequencies : structured numpy array 
        List of calibration frequencies (amplitude and phase params) from PE, generated from calibration group's measurements of detector fluctuations in various (sensitivity-relevant) frequency regimes  
    f_ref : int optional
        Reference Frequency
    f_lower : {None, float}
        Low-frequency cutoff for injected signals. If None, use value
        provided by each injection.
    amp_order : {0, int} 
        pN Amplitude order
    phase_order: {0, int} 
        pN Phase order: -1 yields highest in template
    xml_filename : str 
        path of xml table to use instead of samples (if SimInspiral .xml table already made)
    sampleType : str
        sampleType to make single injection (SimInspiral row) out of; 
        choose singleLogPost (MAP waveform) OR singleLogL (MLL waveform)
    **kwargs : 
        other arguments to be passed down function chain to get_td_waveform (see PyCBC docs for acceptable args.)
    

    Returns
    --------
    signal : float
        h(t) corresponding to the injection.
    """
    if ifos is None:
        ifos = ['H1', 'L1']
    if approx is None:
        approx = 'IMRPhenomPv2pseudoFourPN'
    if fref is None:
        fref = 20
    if amp_order is None:
        amp_order = 0
    if flow is None:
        flow = 20

    if xml_filename is None:
        if samples is None: 
            print('Error: neither xml_filename or samples passed. Please try again')
            sys.exit(1)
        ### injecting MAP by default, from samples
        sim_inspiral = io.BilbySamplesIO.samples2table(samples, approx, flow, amp_order, injfile_name=None, trigtimes=None, sampleType=sampleType)
    else:
        ### ... unless we have another xml table we'd like to use (one row or many - return all strains)
        sim_inspiral = io.SimInspiralIO.parseXML(xml_filename)

    num_samples = int(sample_rate * duration)
    
    ### make strain for each injection
    strains = []

    for ifo in ifos:
        data = []
        for sim in sim_inspiral:
            # create a time series of zeroes to inject waveform into
            initial_array = np.zeros(num_samples)
            strain = pycbc.types.TimeSeries(initial_array,
                                            delta_t=1.0 / sample_rate, epoch=epoch)

            # inject waveform into time series of zeroes
            apply_waveform_to_strain(strain, sim, ifo, f_ref=fref, f_lower=flow, phase_order=phase_order, **kwargs)
            data.append(Strain(strain))
        strains.append(data)

    if calibration_frequencies is not None:
        strains = calibrate_strains_from_samples(strains, samples, calibration_frequencies, ifos)

    return strains
