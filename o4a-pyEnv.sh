#!/bin/bash


# source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh
# conda activate igwn

if [[ "${BASH_SOURCE[0]}" != "${0}" ]]; then
  # The script is being sourced in repo directory
  iwc_repo_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
else
  # The script is being executed directly
  iwc_repo_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")"/.. && pwd)
fi

iwc_pipe_dir=$iwc_repo_dir/iwc_pipe
iwc_hveto_dir=$iwc_pipe_dir/hveto_segs/
iwc_scripts_dir=$iwc_repo_dir/iwc_scripts
iwc_web_utils_dir=$iwc_repo_dir/iwc_web_utils
iwc_package_dir=$iwc_repo_dir/igwn_wave_compare

texlive_bin=/home/johnmichael.sullivan/src/texlive/2023/bin/x86_64-linux

export IWC_REPO=$iwc_repo_dir

export PATH=$iwc_scripts_dir:$iwc_pipe_dir:$texlive_bin:$iwc_hveto_dir:$PATH
export PYTHONPATH=$iwc_scripts_dir:$iwc_web_utils_dir:$iwc_package_dir:$iwc_repo_dir:$PYTHONPATH



