#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Waveforms routines. Updated for O4.
"""

from __future__ import (division, print_function, absolute_import)

import numpy as np
from scipy import interpolate

import pycbc.inject
from .strain import Strain
import lalsimulation as ls
import lal
from pycbc.conversions import tau0_from_mass1_mass2
from pycbc.filter import resample_to_delta_t
from pycbc.waveform import get_td_waveform, get_td_waveform_from_fd, utils as wfutils
from pycbc.types import float64, float32
from pycbc.detector import Detector

from ligo.lw import utils as ligolw_utils 
from ligo.lw import lsctables, ligolw, table

from igwn_wave_compare import iwc_io as io

### MARK DEV: may deprecate depending on use of LALSim. consult.
# Max amplitude orders found in LALSimulation (not accessible from outside of LALSim) */
MAX_NONPRECESSING_AMP_PN_ORDER = 6
MAX_PRECESSING_AMP_PN_ORDER = 3

class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
    pass 


lsctables.use_in(LIGOLWContentHandler)

class Waveform():
    ### MARK DEV: may deprecate (LalSim)
    injection_func_map = {
        np.dtype(float32): ls.SimAddInjectionREAL4TimeSeries,
        np.dtype(float64): ls.SimAddInjectionREAL8TimeSeries
    }
    
    # def legacy_approximant_name(apx):
    #     """Convert the old style xml approximant name to a name
    #     and phase_order. Alex: I hate this function. Please delete this when we
    #     use Collin's new tables.
    #     MARK DEV: Deprecated
    #     """
    #     apx = str(apx)
    #     try:
    #         order = ls.GetOrderFromString(apx)
    #     except:
    #         logging.warning("Warning: Could not read phase order from string, using default")
    #         order = -1
    #     name = ls.GetStringFromApproximant(ls.GetApproximantFromString(apx))
    #     return name, order
    
    def flow_to_fmin(flow, amp_order, approx):
        """
        Compute the minimum frequency for waveform generation using amplitude orders
        above Newtonian.  The waveform generator turns on all orders at the orbital
        associated with fMin, so information from higher orders is not included at fLow
        unless fMin is sufficiently low.

        This function replicates conservative choices made in LALInference
        """
        if amp_order == -1:
            ### MARK DEV: maybe not a bad use of lalsim. use pycbc for other timeseries/wf maniupulation stuff.
            if approx == ls.SpinTaylorT2 or approx == ls.SpinTaylorT4:
                amp_order = MAX_PRECESSING_AMP_PN_ORDER
            else:
                amp_order = MAX_NONPRECESSING_AMP_PN_ORDER
        return flow * 2./(amp_order+2)

    ### MARK DEV: GENERAL: NEED COMMENTS! 
    def calibrate_strain(cls, strain, spcal_logfreqs, spcal_amp, spcal_phase):
        ### if we do not have calibration params
        if len(spcal_logfreqs) == len(spcal_amp) == len(spcal_phase) == 0:
            return strain
        
        ### fit nominal calibration amplitude and phase params to strain data to get corrections (da and dphi)
        da = interpolate.interp1d(spcal_logfreqs, spcal_amp, kind='cubic', bounds_error=False, fill_value="extrapolate")(
            np.log(strain.freqs))
        dphi = interpolate.interp1d(spcal_logfreqs, spcal_phase, kind='cubic', bounds_error=False,
                                    fill_value="extrapolate")(np.log(strain.freqs))

        ### create calibration correction factor - TODO: more info on this arithmetic
        cal_factor = (1.0 + da) * (2.0 + 1j * dphi) / (2.0 - 1j * dphi)
        ### calibrate strain
        strain.data *= cal_factor
        return strain

    ### MARK DEV: GENERAL
    def calibrate_strains_from_samples(cls, strains, samples, calibration_frequencies, ifos):
        sample_params = samples.dtype.names

        cal_strains = []
        for i, ifo in enumerate(ifos):

            # Amplitude calibration model ## if calibrated amplitude in sample params
            amp_params = sorted([param for param in sample_params if
                                ('{0}_spcal_amp'.format(ifo.lower()) in param or
                                '{0}_spcal_amp'.format(ifo) in param)])

            # Phase calibration model ## if calibrated phase in sample params
            phase_params = sorted([param for param in sample_params if
                                ('{0}_spcal_phase'.format(ifo.lower()) in param or
                                    '{0}_spcal_phase'.format(ifo) in param)])

            logfreqs = np.log(calibration_frequencies[ifo])
            data = []
            ifo_strains = strains[i]
            for s, sample in enumerate(samples):
                ### get calibration parameters (amplitude and phase)
                spcal_amp = [sample[param] for param in amp_params]
                spcal_phase = [sample[param] for param in phase_params]
                ### calibrate strains for each sample from calib. params
                data.append(cls.calibrate_strain(ifo_strains[s], logfreqs, spcal_amp, spcal_phase))
            cal_strains.append(data)

        return cal_strains
    
    ### MARK DEV: COMMENTS PLEASE! Why are we using travel time STRAIGHT through earth?
    def apply_waveform_to_strain(cls, strain, inj, detector_name, f_lower=None, distance_scale=1,
                                amp_order=0, injection_sample_rate=None, **kwargs):
        ### MARK DEV: merge wf/project per ifo
        
        '''MARK DEV:
        begin updating injection functionality from here (down stack.)
        '''

        """Add injections (as seen by a particular detector) to a time series.

        Parameters
        ----------
        strain : TimeSeries
            Time series to inject signals into, of type float32 or float64.
        inj: Injection row
            Injection row containing parameters of the required strain
        detector_name : string
            Name of the detector used for projecting injections.
        f_lower : {None, float}, optional
            Low-frequency cutoff for injected signals. If None, use value
            provided by each injection.
        distance_scale: {1, float}, optional
            Factor to scale the distance of an injection with. The default is
            no scaling.
        amp_order: {0, int} optional
            Amplitude order
        injection_sample_rate: float, optional
            The sample rate to generate the signal before injection

        Returns
        -------
        None

        Raises
        ------
        TypeError
            For invalid types of `strain`.
        """
        '''
        a good bit of logic padding out duration from params... etc. Might not be in place down stack, yet, where worth it to update
        '''
        if strain.dtype not in (float32, float64):
            raise TypeError("Strain dtype must be float32 or float64, not "
                            + str(strain.dtype))
        ### MARK DEV: update TS to PYCBC
        lalstrain = strain.lal()  
        ### Dividing the radius of the Earth by the speed of light gives the time taken for the signal to travel the distance equal to the diameter of the Earth.
        ### not a bad check 
        earth_travel_time = lal.REARTH_SI / lal.C_SI
        t0 = float(strain.start_time) - earth_travel_time
        t1 = float(strain.end_time) + earth_travel_time

        # pick lalsimulation injection function... MARK DEV: this is gross and unnecessary; replace PyCBC
        add_injection = cls.injection_func_map[strain.dtype]

        delta_t = strain.delta_t
        if injection_sample_rate is not None:
            delta_t = 1.0 / injection_sample_rate

        f_l = inj.f_lower if f_lower is None else f_lower
        ### MARK DEV: NOTE FROM O3b: roughly estimate if the injection may overlap with the segment (tau0)
        # Add 2s to end_time to account for ringdown and light-travel delay
        end_time = inj.get_time_geocent() + 2 
        inj_length = tau0_from_mass1_mass2(inj.mass1, inj.mass2, f_l)
        
        # Start time is taken as twice approx waveform length with a 1s
        # safety buffer (either side)
        start_time = inj.get_time_geocent() - 2 * (inj_length + 1) 
        
        ### safety check for strain duration of segment 
        if end_time < t0 or start_time > t1:
            raise ValueError("Signal lies outside the 'strain' time window")
        
        signal = cls.make_strain_from_inj_object(inj, delta_t,
                                            detector_name, f_lower=f_l, distance_scale=distance_scale,
                                            amp_order=amp_order, **kwargs) ### MARK DEV (UPDATE MARK): this function, too. Perhaps begin updating here
            
        signal = resample_to_delta_t(signal, strain.delta_t, method='ldas') ### pycbc resample

        if float(signal.start_time) > t1:
            raise ValueError("Signal begins after 'strain' ends")

        ### MARK DEV: replace with PyCBC
        signal = signal.astype(strain.dtype)
        signal_lal = signal.lal()
        add_injection(lalstrain, signal_lal, None)
        
        ### MARK DEV: replace with PyCBC; not a bad way of setting data equal... but no lal objs. like this (MOSTLY FOR THIS FUNCTION)
        strain.data[:] = lalstrain.data.data[:]
        
    def make_strain_from_inj_object(cls, inj, delta_t, detector_name,
                                f_lower=None, distance_scale=1,
                                amp_order=0, **kwargs):
        ### MARK DEV: merge wf/project per ifo
        
        '''MARK DEV: test changes with XML I/O'''
        """Make a h(t) strain time-series from an injection object as read from
        a sim_inspiral table, for example.

        Parameters
        -----------
        inj : injection object
            The injection object to turn into a strain h(t).
        delta_t : float
            Sample rate to make injection at.
        detector_name : string
            Name of the detector used for projecting injections.
        f_lower : {None, float}, optional
            Low-frequency cutoff for injected signals. If None, use value
            provided by each injection.
        amp_order : {0, int} optional
            Amplitude order
        distance_scale: {1, float}, optional
            Factor to scale the distance of an injection with. The default is
            no scaling.

        Returns
        --------
        signal : float
            h(t) corresponding to the injection.
        """
        f_l = inj.f_lower if f_lower is None else f_lower

        ### MARK DEV (FIXME): fix with directly ingesting inj.approximant, inj.phase_order
        name, phase_order = cls.legacy_approximant_name(inj.waveform) ### 
        fmin = cls.flow_to_fmin(f_l, amp_order, name) ### 

        # compute the waveform time series in radiation frame (for current detector)
        '''mark dev: good place to work on **kwargs all the params'''
        hp, hc = get_td_waveform(template=inj, approximant=name,
                                            delta_t=delta_t, phase_order=phase_order,
                                            f_lower=fmin, distance=inj.distance, amplitude_order=amp_order,
                                            **kwargs)
        ### project onto current detector
        return cls.projector(detector_name,
                        inj, hp, hc, distance_scale=distance_scale)


    def projector(cls, detector_name, inj, hp, hc, distance_scale=1):
        ### MARK DEV: merge wf/project per ifo
        
        """
        Use the injection row to project the polarizations into the
        detector frame
        """
        detector = Detector(detector_name)

        hp /= distance_scale
        hc /= distance_scale

        try:
            tc = inj.tc
            ra = inj.ra
            dec = inj.dec
        except:
            tc = inj.get_time_geocent()

            ra = inj.longitude
            dec = inj.latitude

        hp.start_time += tc
        hc.start_time += tc

        # taper the polarizations
        ### taper horizontal and cross TS in radiation frame before projection
        try:
            hp_tapered = wfutils.taper_timeseries(hp, inj.taper)
            hc_tapered = wfutils.taper_timeseries(hc, inj.taper)
        except AttributeError:
            ### by default, in injections made for IWC, taper set to TAPER_NONE 
            hp_tapered = hp
            hc_tapered = hc

        ### taking this out unless we need it here... 
        # projection_method = 'lal'
        # if hasattr(inj, 'detector_projection_method'): ### (is not blank)
        #     projection_method = inj.detector_projection_method
        #     '''Mark Dev: SimInspiral object will probably pitch a fit when it gets here. Not a valid SimInspiral column?'''

        # logging.info('Injecting at %s, method is %s', tc, projection_method)

        # compute the detector response and add it to the strain
        signal = detector.project_wave(hp_tapered, hc_tapered,
                                    ra, dec, inj.polarization)
        return signal ### here is when we finally get signal
    
    def generate_strains_from_samples(cls, samples, sample_rate=16384., duration=8., epoch=0., ifos=None,
                                  flow=None, approx=None, calibration_frequencies=None,
                                  fref=None, amp_order=None, xml_filename=None, sampleType='singleLogPost', **kwargs):
        ### residuals analysis only passes in one sample
        
        if ifos is None:
            ifos = ['H1', 'L1']
        if approx is None:
            approx = 'IMRPhenomPv2pseudoFourPN'
        if fref is None:
            fref = 20
        if amp_order is None:
            amp_order = 0
        if flow is None:
            flow = 20

        if xml_filename is None:
            ### injecting MAP by default, from samples
            sim_inspiral = io.BilbySamplesIO.samples2table(samples, approx, flow, amp_order, injfile_name=None, trigtimes=None, sampleType='singleLogPost')
        else:
            ### ... unless we have another xml table we'd like to use (one row or many - return all strains)
            sim_inspiral = io.SimInspiralIO.parseXML(xml_filename)

        num_samples = int(sample_rate * duration)
        
        ### make strain for each injection
        strains = []

        ### MARK DEV (FIXME): change nested order of ifo and sim_inspiral loop
        for ifo in ifos:
            data = []
            for sim in sim_inspiral:
                # create a time series of zeroes to inject waveform into
                initial_array = np.zeros(num_samples)
                strain = pycbc.types.TimeSeries(initial_array,
                                                delta_t=1.0 / sample_rate, epoch=epoch)

                # inject waveform into time series of zeroes
                '''MARK DEV: begin updating with latest PyCBC code in apply_waveform_to_strain'''
                cls.apply_waveform_to_strain(strain, sim, ifo, f_ref=fref, f_lower=flow)
                data.append(Strain(strain))
            strains.append(data)
        # shape: [ifo[sim]]
        if calibration_frequencies is not None:
            strains = cls.calibrate_strains_from_samples(strains, samples, calibration_frequencies, ifos)

        return strains
        