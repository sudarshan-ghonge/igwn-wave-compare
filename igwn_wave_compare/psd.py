#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
PSD-related utilities.
"""

import numpy as np
import sys
from .utils import BoundedInterp1D
import pycbc

def interp_from_txt(infile, flow=None, fhigh=None, asd_file=False):
    """Read PSD/ASD from a text file"""
    freqs, psd = np.loadtxt(infile, unpack=True)
    if asd_file:
        psd = np.square(psd)
    return BoundedInterp1D(freqs, psd, low=flow, high=fhigh, below_domain_val=np.inf, above_domain_val=np.inf)

def interp_bayesline_from_txt(infile, ignore_lines=False, flow=None, fhigh=None):
    """Read a bayesline PSD from file, with the option of ignoring lines"""
    freqs, _, spline, line = np.loadtxt(infile, unpack=True)

    psd = spline if ignore_lines else spline + line
    psd /= 2.

    return BoundedInterp1D(freqs, psd, low=flow, high=fhigh, below_domain_val=np.inf, above_domain_val=np.inf)


def interpolate_psds_fromWF(h_list, psd_interpolants=None):
    '''
    function to interpolate PSD's. Uses igwn_wave_compare.psd.interp_from_txt method, 
    and pass psd_interpolants to this function [igwn_wave_compare.utils.BoundedInterp1D].
    
    Parameters
    ----------
    h_list : list<pycbc.types.FrequencySeries]
        List of PE Max Log Likelihood Waveforms (frequency domain) to extract PSD interpolation parameters out of.
    psd_interpolants: list[igwn_wave_compare.utils.BoundedInterp1D]:
        List of interpolants to use to make PSD's. Assume uses interp_bayesline_from_txt or interp_from_txt to create these.
    
    Returns
    -------
    psd_FS: list[pycbc.types.FrequencySeries]
        optimal network snr
 
    '''
    
    if psd_interpolants == None:
        print('Error: list of psd interpolants not passed. Check arguments and try again.')
        sys.exit(1)
    
    if isinstance(psd_interpolants, list):
        for ind, p in enumerate(psd_interpolants):
            if not isinstance(p, BoundedInterp1D):
                print(f'Error: PSD interpolant for {args.ifos[ind]} not of type iwc.utils.BoundedInterp1D. Check args and try again.')
                sys.exit(1)
    
    elif psd_interpolants == None:
        print('Error: list of psd interpolants not passed. Check arguments and try again.')
        sys.exit(1)
    
    ### extract freq. info about waveforms; assume sampled consistently.
    # h_list_F = [h.to_frequencyseries() for h in h_list]
    delta_f = h_list[0].delta_f
    freqs = h_list[0].sample_frequencies  
    
    ### interpolate PSD's to WF specifications; convert to FrequencySeries for optimal SNR calculation. 
    psds_interp = [psd(freqs) for psd in psd_interpolants]
    psd_FS = [pycbc.types.frequencyseries.FrequencySeries(psds_i, delta_f=delta_f) for psds_i in psds_interp]
    return psd_FS