
# syntax=docker/dockerfile:1
FROM igwn/lalsuite-dev:bullseye

LABEL name="Igwn Wave Compare: Base Distribution" \
      maintainer="Jack Sullivan <johnmichael.sullivan@ligo.org>" \
      date="20230716" \
      support="Experimental Platform"

# Create both users (admin and analysis); last one (root) is so we can set things up properly
# USER root 

WORKDIR /app

RUN apt-get update -y && apt-get upgrade -y && apt-get -y install python3.10 && apt-get -y install python3-scipy 
RUN curl -sS https://bootstrap.pypa.io/get-pip.py | python3.10

# && apt-get -y install python3.10 && apt-get -y install python3-pip


# Setup directories for bind mounting (direct reading on file host[execute nodes])
RUN mkdir -p /cvmfs /hdfs /gpfs /ceph /hadoop /etc/condor

# copy contents igwn-wave-compare to /src in container
COPY . /src

COPY container/entrypoint.sh /usr/local/bin/
RUN ln -s usr/local/bin/entrypoint.sh / # backwards compat
ENTRYPOINT ["entrypoint.sh"] 

# Python/pip/setuptools configured using base image
RUN pip3 -m pip install -r /src/requirements_container.txt

# install igwn_wave_compare`
RUN pip3 -m pip install /src/

# ## create analysis user and make them default at entry
# RUN useradd -ms /bin/bash analy.user
# USER analy.user


