#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#               2017-2020 Sudarshan Ghonge <sudarshan.ghonge@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Summarize the residuals analysis.  

Loads in the network snr samples for the residuals analysis.
The SNR distribution can be transformed to a fitting-factor distribution to form
the same GR test as was performed for GW150914.

This code also supports background analysis.  We compute CDFs for 90th percentile SNRs and compare the on-source results to get
a p-value.
"""

import sys
import os

import numpy as np

import matplotlib

matplotlib.use("Agg")   
from matplotlib import pyplot as plt
### TURN OFF 
plt.rc('text', usetex=False)
plt.rc('font', family='serif')
# plt.rc('text', usetex=False)

from matplotlib.ticker import ScalarFormatter
import matplotlib.patches as mpatches


import glob
import traceback

import argparse

fig_width_pt = 4 * 246.0  # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 2.0 / 72.27  # Convert pt to inches
golden_mean = (np.sqrt(5) - 1.0) / 2.0  # Aesthetic ratio
fig_width = fig_width_pt * inches_per_pt  # width in inches
fig_height = fig_width * golden_mean  # height in inches
fig_size = [fig_width, fig_height]
fontsize = 70
params = {
    'axes.labelsize': fontsize,
    'font.size': fontsize,
    'legend.fontsize': fontsize,
    'xtick.color': 'k',
    'xtick.labelsize': fontsize,
    'ytick.color': 'k',
    'ytick.labelsize': fontsize,
    'text.usetex': False,
    'text.color': 'k',
    'figure.figsize': fig_size
}

plt.rcParams.update(params)


def update_progress(progress):
    print('\r\r[{0}] {1}%'.format('#' * (progress / 2) + ' ' * (50 - progress / 2), progress), end=' ')
    if progress == 100:
        print("\nDone")
    sys.stdout.flush()


# def parse_evidence(evidence_path):
#     f = open(os.path.join(evidence_path, 'evidence.dat'), 'r')
#     evidence = dict()
#     for line in f.readlines():
#         parts = line.split()
#         evidence[parts[0]] = float(parts[1])
#     f.close()
#     logBsn = evidence['signal'] - evidence['noise']
#     logBsg = evidence['signal'] - evidence['glitch']
#     return evidence, logBsn, logBsg


def compute_netsnr(moments_path, ifos):
    """
    Extract the 90th percentile of the network snr
    """
    momentsfiles = [os.path.join(
        moments_path, "signal_whitened_moments_{}.dat".format(i)) for i in
        ifos]
    moments = [np.loadtxt(file) for file in momentsfiles]
    net_snrs = 0
    ifo_snrs = []
    for i in range(len(ifos)):
        net_snrs += moments[i][:, 0] ** 2
        ifo_snrs.append(moments[i][:, 0])

    net_snrs = np.sqrt(net_snrs)

    return net_snrs, ifo_snrs


def snr_percentile(moments_path, ifos):
    """
    Extract the 90th percentile of the network snr
    """
    momentsfiles = [os.path.join(
        moments_path, "signal_whitened_moments_{}.dat".format(i)) for i in
        ifos]
    try:
        moments = [np.loadtxt(file) for file in momentsfiles]
    except:
        return 0
    net_snrs = 0
    for i in range(len(ifos)):
        net_snrs += moments[i][:, 0] ** 2

    net_snrs = np.sqrt(net_snrs)

    snrs = np.percentile(net_snrs, [10, 50, 90])
    return snrs


def ecdf(samples):
    """
    Return the empirical CDF of the distribution from which samples are drawn
    """
    sorted = np.sort(samples)
    return sorted, np.arange(len(sorted)) / float(len(sorted))


def fitfactor(snrdet, snrres):
    """
    Compute lower limit on fitting factor between MAP and event

    From GW150914 testing-GR:
    snr2res = snr2det(1-ff2)/ff2
    (1-ff2)/ff2 = snr2res / snr2det
    snr2res/snr2det*ff2 = 1 -ff2
    (snr2res/snr2det + 1)*ff2 = 1
    ff2 = 1/(snr2res/snr2det + 1)
    ff2 = snr2det/(snr2det + snr2res)
    """
    snr2det = snrdet ** 2
    snr2res = snrres ** 2
    ff2 = snr2det / (snr2det + snr2res)
    return np.sqrt(ff2)


def get_waveform(filename):
    """
    Load BW waveform and CI's from .dat file.
    """
    names = ['samples','median_waveform','50low','50high','90low','90high']
    data = np.recfromtxt(filename,names=names)
    return (data['samples'],data['median_waveform'],data['50low'],data['50high'],data['90low'],data['90high'])

def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--onsource-path", default=None, type=str,
                        required=True, help="""Path to BayesWave residuals files (e.g.,
            evidence.dat, signal_whitened_moments)""")
    parser.add_argument("--onsource-signal-path", default=None, type=str,
                        help="Path to the BayesWave run on signal")
    parser.add_argument("--offsource-path", default=None, type=str,
                        required=False, help="""Path to output directories labelled with GPS
            times which contain the BayesWave results for off-source trials.
            Used for p-values""")
    parser.add_argument("--output-path", default='.', type=str, required=False,
                        help="Where to save figures and summary info")
    parser.add_argument("--make-plots", default=False, action="store_true")
    parser.add_argument("--intermediate-run", default=False,
                        action="store_true")
    # parser.add_argument("--ml-snr", default=12.6760132186, type=float,
    #                     help="""The network SNR of the BBH ML waveform used to construct the
    # residuals""")
    parser.add_argument("--ifos", default=None, type=str, help="IFOs which saw this event", nargs='+',
                        choices=['H1', 'L1', 'V1'], required=True)    
    parser.add_argument("--nruns", default=None, type=int,
                        help="Number of background runs to consider. Should be less than the total number of background runs")
    parser.add_argument("--singularity-dir-make", default=False, action="store_true",
                        help="Make output directories in container for binding to output.")
    # parser.add_argument("--dev-plot-bypass", default=False, action="store_true", help='Bypass plotting to do locally (manually). Due to issues with Tex files in condor env (matplotlib).')
    
    parser.add_argument("--simulated-onsource-residuals", default=False, action="store_true",
                        help="Simulated onsource residuals (simulated PSD's); no offsource background.")
    
    opts = parser.parse_args()

    return opts

def main(opts):
    opts = parser()

    ifos = opts.ifos
    
    if not os.path.isdir(os.getcwd(), opts.output_path):
        os.makedirs(os.getcwd(), opts.output_path)
        
    # On-source reconstruction BW run
    if opts.onsource_path is not None:
        ### Compute SNRs
        onsource_netsnr, onsource_ifosnrs = compute_netsnr(os.path.join(opts.onsource_path, 'post', 'signal'), ifos)
        ### Compute intervals for onsource and single detector snr 
        SNRintervals = np.percentile(onsource_netsnr, [10, 50, 90])
        onsource_snrmedian = SNRintervals[1]
        onsource_snr90 = SNRintervals[2]
                
        ### reading in optimal network matched filter SNR computed using iwc_residuals.py 
        with open('residual_frame/SNR_GR.txt', 'r+') as f:
            snr_gr_read = f.read().strip()
        try:
            snr_gr = float(snr_gr_read)
        except:
            print(f'Error: SNR_GR {snr_gr} could not be read as a float. Check y-our data and try again.')
            sys.exit(1)
        print('SNR GR: ', snr_gr)

        f = open(os.path.join(opts.output_path, "burst-TGR-summary.txt"), 'w')
        f.writelines(f"onsource netSNR: {0:.5f} {1:.5f} {2:.5f}\n".format(SNRintervals[0],
                                                                            SNRintervals[1], SNRintervals[2]))
        
        onsource_netsnr = onsource_netsnr[~(onsource_netsnr > snr_gr)]
        
        ### compute fitting factor distribution as specified in original GW150914 paper, GWTC-1 TGR paper
        FFdist = np.array([fitfactor(snr_gr, netsnr) for netsnr in
                            onsource_netsnr])
    
        ### compute fitting factor intervals
        FFintervals = np.percentile(FFdist, [10, 50, 90])
        f.writelines("CBC MAP netSNR: {:.5f}\n".format(snr_gr))
        f.writelines("Fitting factor intervals: {0:.5f} {1:.5f} {2:.5f}\n".format(FFintervals[0],
                                                                                FFintervals[1], FFintervals[2]))
        f.writelines("GR inaccuracy < {:.0f}%\n".format(100 * (1 - FFintervals[0])))
        f.close()
        
        
    # On-source signal data BW run
    if opts.onsource_signal_path is not None:
        try:
            ### gather evidences for each competing residuals  model
            onsource_signal_netsnr, onsource_signal_ifosnrs = compute_netsnr(
                os.path.join(opts.onsource_signal_path, 'post', 'signal'), ifos)
            SNRintervals_signal = np.percentile(onsource_signal_netsnr, [10, 50, 90])
            onsource_signal_snrmedian = SNRintervals_signal[1]
            onsource_signal_snr90 = SNRintervals_signal[2]
        except:
            onsource_signal_snrmedian = 0
            onsource_signal_snr90 = 0
    else:
        onsource_signal_snrmedian = 0
        onsource_signal_snr90 = 0
    ####################################


    if opts.make_plots:
        nrows=2
        figsize=(fig_size[0] * 0.5 * len(ifos), 1.4 * fig_size[1] * 0.5 * len(ifos))
        f, ax = plt.subplots(nrows=nrows,
                                figsize=figsize)
        

        ### plot SNR histograms and fitting factor distribution
        ax[0].hist(onsource_netsnr, bins=100, alpha=1,
                label='Network SNR', histtype='stepfilled')
        for i in range(len(ifos)):
            ax[0].hist(onsource_ifosnrs[i], bins=100, label="$%s_{SNR}$" % ifos[i],
                        histtype='stepfilled', alpha=0.75)
        ax[0].axvline(SNRintervals[2], label='$SNR_{{90}}$ = {0:.2f}'.format(onsource_snr90), color='k', lw=8.0)

        ax[0].legend(loc='upper right')
        ax[0].set_xlabel('Signal-to-Noise Ratio', color='k', fontsize=fontsize)
        ax[0].set_ylabel('PDF', color='k', fontsize=fontsize)
        ax[0].minorticks_on()
        ax[1].hist(FFdist, bins=100, histtype='stepfilled')
        ax[1].axvline(FFintervals[0], label='90% L.L.$', linestyle='--',
                        color='k')
        ax[1].axvline(FFintervals[1], label='Median', linestyle='-',
                        color='k')
        ax[1].axvline(FFintervals[2], linestyle='--', color='k')
        ax[1].set_xlabel('Fitting Factor', color='k', fontsize=fontsize)
        ax[1].set_ylabel('PDF', color='k', fontsize=fontsize)
        ax[1].legend(loc='upper left', fontsize=fontsize)
        ax[1].minorticks_on()

        f.tight_layout()

        f.savefig(os.path.join(opts.output_path, 'residual-snr_FF-distributions.png'))
        plt.close('all')
    
    if opts.simulated_onsource_residuals:
        ### make refined BW reconstruction plots
        if opts.onsource_path is None: 
            print('Error: --onsource-path not passed. Check args and try again.')
            sys.exit(1)
            
        wf_dats = {}
        for ind, ifo in enumerate(ifos):
            signal_filename = os.path.join(opts.onsource_path, f'post/signal/signal_median_time_domain_waveform_{ifo}.dat')
            timesamp,median_waveform, high_50, low_50, high_90, low_90 = get_waveform(signal_filename)
            ### save signalOnly reconstruction data per ifo
            wf_dats[ifo] = {
                'timesamp': timesamp,
                'median_waveform': median_waveform,
                'high_50': high_50,
                'low_50': low_50,
                'high_90': high_90,
                'low_90': low_90
            }
        ### load time
        time_addr = os.path.join(opts.onsource_path, 'post/timesamp.dat')
        time = np.loadtxt(time_addr)
        zero_ind = np.where(time == 0.0)
        ### colors for plotting
        mdc_colour = 'teal'
        signalOnly_color = 'darkorchid'
        
        ### scientific notation on y-axis of each plot
        formatter = ScalarFormatter(useMathText=True)
        formatter.set_scientific(True) 
        
        ### plot BW residuals reconstructions
        for ifo in ifos:
            plt.figure()

            fig, ax = plt.subplots()

            ### injected MDC waveform
            inj_filename = os.path.join(opts.onsource_path, f'post/injected_whitened_waveform_{ifo}.dat')
            inj_median_waveform = np.genfromtxt(inj_filename)

            ax.plot(time, inj_median_waveform, mdc_colour, linewidth=1, alpha=0.5, label='Injected Residuals')
            
            ### signal reconstruction
            ax.fill_between(time, wf_dats[ifo]['low_50'], wf_dats[ifo]['high_50'], facecolor=signalOnly_color, edgecolor=signalOnly_color, alpha=0.5)
            ax.fill_between(time, wf_dats[ifo]['low_90'], wf_dats[ifo]['high_90'], facecolor=signalOnly_color, edgecolor=signalOnly_color, alpha=0.3)
            ax.plot(time, median_waveform, color=signalOnly_color, linewidth=1, alpha=1, label='BayesWave Reconstruction')
            ax.set_xlabel('Time (s)', labelpad=20)
            ax.set_ylabel('$\sigma_{noise}$', labelpad=20)
            
            ax.yaxis.set_major_formatter(formatter)

            
            plt.title(f'{ifo}: Reconstructed Residuals')
            plt.grid(True)       
            fig.tight_layout()
            
            plt.savefig(os.path.join(opts.output_path, f'{ifo}_bw_recon_plot_fitInj.png'))
            fig.tight_layout(rect=[0, 0, 1, 0.95])
            ### IF FIRST IFO: INCLUDE LEGEND
            if ifo == 'H1':
                inj_patch = mpatches.Patch(color=mdc_colour, label='Injected Residuals')
                sigOnly_patch = mpatches.Patch(color=signalOnly_color, label='BayesWave Reconstruction')
                plt.legend(handles=[inj_patch, sigOnly_patch], loc='upper center', bbox_to_anchor=(0.5, 1.3), ncol=2, fontsize=45)
            lower_ylim = 1.5 * wf_dats[ifo]['low_90'][zero_ind]
            upper_ylim = 1.5 * wf_dats[ifo]['high_90'][zero_ind] 
            ax.set_ylim(lower_ylim, upper_ylim)
            plt.subplots_adjust(left=0.2)
            plt.savefig(os.path.join(opts.output_path, f'{ifo}_bw_recon_plot_fitRecon.png'))
            plt.close('all')

    ### OFF-SOURCE ###
    if not opts.simulated_onsource_residuals:
        offsource_dirs =  glob.glob(os.path.join(opts.offsource_path, 'trigtime_1*')) # + glob.glob(os.path.join(opts.offsource_path, 'bayeswave_1*'))
        offsource_times = []
        offsource_snr90 = []
        offsource_snrmedian = []
        print("Bad runs: ")
        idx = np.random.permutation(len(offsource_dirs))
        if opts.nruns is None:
            nruns = len(offsource_dirs)
        else:
            nruns = opts.nruns
        run_count = 0
        for d, di in enumerate(np.array(offsource_dirs)[idx]):
            try:
                if not run_count < nruns:
                    break
                snrs = snr_percentile(os.path.join(di, 'post', 'signal'), ifos)
                snrmedian = snrs[1]
                snr90 = snrs[2]
                if(snr90>25):
                    raise Exception("SNR Value too high. Run has errors")
                offsource_snr90.append(snr90)
                offsource_snrmedian.append(snrmedian)
                offsource_times.append(float(di.split('/')[-1].split('_')[1]))
                run_count += 1

            except Exception as e:
                print(e, di)
                print(traceback.format_exc())
            offsource_times = list(set(offsource_times))

        offsource_snr90_sorted, snr90_ecdf = ecdf(offsource_snr90)
        offsource_snrmedian_sorted, snrmedian_ecdf = ecdf(offsource_snrmedian)
        np.savetxt(os.path.join(opts.output_path,"offsource_snr_90s.dat"), offsource_snr90_sorted.T, fmt="%.3f")
        ### save amount runs done
        with open (os.path.join(opts.output_path,"runCount.txt"), 'w+') as f:
            f.write(f'{run_count}/{nruns}')
            f.close()
            
            
        if opts.onsource_path is not None:
            p_snr90 = len(np.where(offsource_snr90 > onsource_snr90)[0]) / float(len(offsource_snr90))
            
            ### Save pertinent data 
            f = open(os.path.join(opts.output_path, "p-value.txt"), 'w')
            f.writelines("{0:.2f}".format(p_snr90))
            f.close()
            np.savez(os.path.join(opts.output_path, 'offsource_residual_stats'), 

                        offsnrmedian=(offsource_snrmedian_sorted, snrmedian_ecdf),
                        offsnr90=(offsource_snr90_sorted, snr90_ecdf),

                        offsource_times=offsource_times,
                        onsnr90Signal=onsource_signal_snr90,
                        onsnrmedian_signal=onsource_signal_snrmedian,
                        onsource_cbc_snr=opts.ml_snr, ### SNR_GR is given
                        p_snr90=p_snr90)
        if opts.make_plots:
            ### plot annihilation function
            f, ax = plt.subplots()

            ax.plot(offsource_snr90_sorted, 1 - snr90_ecdf, lw=4.0, label='Offsource $SNR_{90}$')
            ax.set_xlabel('$SNR_{90}$')

            ax.set_ylim(0, 1)
            ax.minorticks_on()
            ax.set_ylabel('1 - CDF')
            ax.grid(linestyle='-', color='grey')
            if opts.onsource_path is not None:
                ax.axvline(onsource_snr90, linestyle='--', lw=4.0, color='red', label='Onsource $SNR_{90}$')
                props = dict(boxstyle='round', facecolor='grey', alpha=0.5)
                textstr = 'p-value = %.2f' % p_snr90
                # place a text box in upper left in axes coords
                ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=fontsize,
                        verticalalignment='top', bbox=props)
            
            ax.legend(loc='upper right', fontsize=fontsize)

            f.tight_layout()

            plt.savefig(os.path.join(opts.output_path, 'snr_residuals.png'))
            # plt.savefig(os.path.join(opts.output_path, 'snr_residuals.pdf'))
            plt.close('all')
    
    ### leaving logic in to lift for external TGR codebase   
    # if opts.make_plots and opts.dev_plot_bypass:
    #     ### adding this bypass option 
    #     # export data in script (not called from within function/module) to dat dictionary
    #     print('Bypassing plot generation due to env. issues with matplotlib. Remove --dev-plot-bypass once this is resolved (in iwc_config: [python_analy_options][dev_plots_bypass]).')
    #     print('Onsource Residuals/TGR analysis continues from here')
    #     # savDat = {name: value for name, value in globals().items() if not name.startswith('__') and not callable(name) and name != 'file'}

    #     needDat = ['args','epoch_padded','duration_padded','delta_t','ifos','times','psds','data','residuals','waveforms','dataF','waveformF','residualsF','waveforms_wt','waveforms_wf','inFrames']
    #     savDat = {name: globals().get(name) for name in needDat}
    #     # savDat = {name: value for name, value in globals().items() if not '__' not in name and not callable(value) and not isinstance(value, types.ModuleType) and name != 'file'}

    #     save(savDat, os.path.join(opts.output_path, 'testgr-analy-dat.p'))

# if __name__ == "__main__":
#     #
#     # Load data
#     #
#     opts = parser()
#     try:
#         main(opts)
#     except Exception as e:
#         if opts.intermediate_run:
#             print('Intermediate Run: Printing Error Below')
#             print(e)
#             sys.exit(0)
#         else:
#             raise e
