import os
import sys
from glob import glob
from datetime import datetime, timedelta
import re
import ligo.segments as segments
import h5py as h5
from numpy.lib import recfunctions as rfn
import numpy as np

import argparse


def gps_to_datetime(gps_time):
    """Converts GPS time to datetime."""
    return datetime(1980, 1, 6) + timedelta(seconds=(gps_time-18)) # (num leap secs )

def datetime_to_str(dt):
    """Converts datetime to string in format YYYYMMDD."""
    return dt.strftime("%Y%m%d")

def datetime_to_str_ext(dt):
    """Converts datetime to string in format YYYYMMDD."""
    return dt.strftime("%Y%m%d-%H:%M")

def is_day_boundary(gps_time):
    """Checks if GPS time lies on the boundary of a day."""
    dt = gps_to_datetime(gps_time)
    return dt.time() == datetime.time(datetime.min)


parser = argparse.ArgumentParser(description=__doc__,
                                formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("--beg-secs", type=float, help="Bounds of time used for offsource analysis.")
parser.add_argument("--end-secs", type=float, help="Bounds of time used for offsource analysis.")
parser.add_argument("--output-file-name", type=str, required=True, help="Path to PE config file.", )

opts = parser.parse_args()

if not opts.beg_secs and opts.end_secs:
    print('Error: --beg-secs and --end-secs not both passed. Please try again.')
    sys.exit(1)
### bounds of our offsource analysis
sampBounds = [opts.beg_secs, opts.end_secs]

### get day, month, year properly formatted
sb = [datetime_to_str(gps_to_datetime(t)) for t in sampBounds]
### time range is in one day

runDirs = None
### if offsource analysis time is all in same day
if sb[0] == sb[1]:

    dayBase = f'/home/detchar/public_html/hveto/day/{sb[0]}'
    day_sp = f'{dayBase}/*'
    timeDirs= glob(day_sp)
    baseTimeDirs = [os.path.basename(t) for t in timeDirs]

    # Check if the string contains the pattern
    extOne = False
    ### Use regex to see if we have an hveto run
    for b in baseTimeDirs:
        if re.fullmatch(r'\d+\.?\d*-\d+\.?\d*', b):
            extOne = True
    
    ### if hveto run not found, exit; ### ARGS: Indicate which IFO for display purposes
    if extOne == False:
        print(f'Error: No valid runs for {sb[0]} found. Data may not exist. Exiting')
        sys.exit(1)

    dirUpd = []
    inds = None
    for ind, t in enumerate(baseTimeDirs):
        ### no, need add this baseTimeDir to
        if t != 'latest' and re.fullmatch(r'\d+\.?\d*-\d+\.?\d*', t):
            secs = [int(s) for s in t.split('-')]
            dates = [datetime_to_str_ext(gps_to_datetime(s)) for s in secs]
            # dur = secs[1] - secs[0]
            ### if our hveto rundir(s) contain the analysis interval (can have multiple) - store
            if secs[0] < sampBounds[0] and sampBounds[1] < secs[1]:
                ### get all hveto Runs within our bounds: leave in list
                dirUpd.append(os.path.join(dayBase, t))
    runDirs = dirUpd
else:
    ### occurs across the boundary of a day
    ### get day dirs
    dirUpd = []
    dayBase = f'/home/detchar/public_html/hveto/day'
    day_sp = [f'{dayBase}/{b}/*' for b in sb]


    ### offsource analysis spread out over 2 days (maximum) (~16388 seconds total)
    ### we want those runs that only go to end of day (TODO: later)
    for ind, d in enumerate(day_sp):
        ### get all hveto runs for a single day
        timeDirs= glob(d)
        baseTimeDirs = [os.path.basename(t) for t in timeDirs]

        extOne = False
        ### Use regex to see if we have an hveto run in this particular day (regex)
        for b in baseTimeDirs:
            if re.fullmatch(r'\d+\.?\d*-\d+\.?\d*', b):
                extOne = True
        ### if hveto run not found, exit; ### ARGS: Indicate which IFO for display purposes
        if extOne == False:
            print(f'Error: No valid runs for {day_sp[ind]} found. Data may not exist. Exiting')
            sys.exit(1)
        dirUpd_day = []
        inds = None
        for hdex, t in enumerate(baseTimeDirs):
            ### if we're in one of the properly formatted rundirs (grab all)
            if t != 'latest' and re.fullmatch(r'\d+\.?\d*-\d+\.?\d*', t):

                ### boundaries of hveto run (from dir basename)
                secs = [int(s) for s in t.split('-')]

                ### DAY BOUNDARY
                ### end boundary of hveto run (first day) must have time of 00:00 for hours, minutes (day boundary); analy. boundary must be EARLIER; get all runs that are
                ### beg boundary of hveto run (second day) must have time of 00:00 for hours, minutes (day boundary); analy. boundary must be LATER; get all runs that are

                if ind == 0: ### day 1
                    ### end boundary at a day boundary for day 1: use all matching hveto runs
                    if is_day_boundary(secs[1]) and sampBounds[0] < secs[1]:
                        dirUpd.append(timeDirs[hdex])
                elif ind == 1:
                    if is_day_boundary(secs[0]) and secs[0] < sampBounds[1]:
                        ### day 2 run directory is on boundary of a day and sample bounds stretch over it: use all matching hveto runs
                        dirUpd.append(timeDirs[hdex])
                else:
                    continue

            # dirUpd.append(dirUpd_day)
            
    runDirs = dirUpd
    
runStr = "\n".join(runDirs)
print(f'Using hveto runs: {runStr}')

### loading segments
def convert_seg_array(arr):
    start_time_s = arr['start_time'] + arr['start_time_ns'] * 1e-9
    end_time_s = arr['end_time'] + arr['end_time_ns'] * 1e-9
    
    new_arr = np.zeros(arr.shape, dtype=[('start_time_s', 'float64'), ('end_time_s', 'float64')])
    new_arr['start_time_s'] = start_time_s
    new_arr['end_time_s'] = end_time_s
    
    return new_arr

allSeg_arr = []
for r in runDirs:
    h5_seg_file = glob(f'{r}/segments/*.h5')
    if len(h5_seg_file)==1:
        h5_seg_file = h5_seg_file[0]
        f = h5.File(h5_seg_file, 'r')
        # segDir = segments.segmentlist()
        runSeg_arrs = []
        for k in f.keys():
            if 'HVT-ROUND' in k:
                proc_seg_array = convert_seg_array(f[f'{k}/active'][:])
                runSeg_arrs.append(proc_seg_array)
        runSeg_combArr = np.concatenate(runSeg_arrs, axis=0)
        allSeg_arr.append(runSeg_combArr)
allSeg_combArr = np.concatenate(allSeg_arr, axis=0)

# print(allSeg_combArr)
print('finished combining segs for pertinent hveto runs into numpy')
print('Coalescing segment list of hveto segments and excluding those outside of offsource analysis window...')

# reformatting for unpacking into segmentlist
start_times = allSeg_combArr['start_time_s']
end_times = allSeg_combArr['end_time_s']
hveto_sl = segments.segmentlist([segments.segment(start, end) for start, end in zip(start_times, end_times)])
### coalesce segments with common time
hveto_sl.coalesce()
bounds = segments.segmentlist([segments.segment(start, end) for start, end in [sampBounds]])
segments_analysis = bounds & hveto_sl
### goes from 885 to 27


segsPath = os.path.expanduser('~/hveto-eventSegs-O4')
if not os.path.exists(segsPath):
    os.makedirs(segsPath)
    
### ARG: event name
with open(os.path.join(segsPath, f'{opts.output_file_name}'), 'w+') as f:
    for v in hveto_sl:
        f.write(f'{v[0]} {v[1]}\n')
    f.close()
print('Finished writing segments to file. EXIT0')

