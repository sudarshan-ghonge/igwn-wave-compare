#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Install script for package"""

import os
from distutils.core import setup
from subprocess import check_output, CalledProcessError

GIT_DESCRIBE = 'git describe --tags --long --dirty'
GIT_VERSION_FMT = '{tag}.{commitcount}+{gitsha}'


def format_version(version, fmt=GIT_VERSION_FMT):
    """
    Format the version string
    """
    parts = version.split('-')
    assert len(parts) in (3, 4)
    dirty = len(parts) == 4
    tag, count, sha = parts[:3]
    if count == '0' and not dirty:
        return tag
    return fmt.format(tag=tag, commitcount=count, gitsha=sha.lstrip('g'))


def get_git_version():
    """
    Get the git version
    """
    git_version = check_output(GIT_DESCRIBE.split()).decode('utf-8').strip()
    return format_version(version=git_version)


try:
    VERSION = os.environ['CI_COMMIT_TAG']
except KeyError:
    try:
        VERSION = os.environ['CI_COMMIT_SHORT_SHA']
    except KeyError:
        try:
            VERSION = get_git_version()
        except (OSError, CalledProcessError):
            with open('VERSION', 'r') as vp:
                VERSION = vp.read().strip()

with open('VERSION', 'w') as vp:
    vp.writelines(VERSION + '\n')

with open("requirements.txt", 'r') as freq:
    LINES = freq.readlines()
    for LINE in enumerate(LINES):
        LINE = LINE[1].rstrip()
REQUIREMENTS = {"install": LINES}

setup(
    name='igwn_wave_compare',  # How you named your package folder (MyLib)
    packages=['igwn_wave_compare', 'iwc_web_utils', 'iwc_pipe', 'iwc_pipe.hveto_segs'],  # Chose the same as "name"
    version=VERSION,  # Start with a small number and increase it with every change you make
    license='GPL',  # Chose a license from here: https://help.github.com/articles/licensing-a-repository
    description='Utilities and libraries for waveform reconstruction comparisons and residuals analysis',
    # Give a short description about your library
    author='Jack Sullivan',  # Type in your name
    author_email='johnmichael.sullivan@ligo.org',  # Type in your E-Mail
    url='https://git.ligo.org/bayeswave/igwn-wave-compare',
    # Provide either the link to your github or to your website
    download_url='https://git.ligo.org/sudarshan-ghonge/bayeswave/-/archive/master/igwn-wave-compare-master.tar.gz',
    # I explain this later on
    keywords=['Burst', 'CBC', 'Waveform Reconstruction', 'Residuals', 'LIGO Toolkit', 'Tests of GR'],  # Keywords that define your package best
    # install_requires=REQUIREMENTS["install"],
    scripts=['iwc_scripts/iwc_reclal.py', 'iwc_scripts/iwc_residuals.py',
              'iwc_scripts/iwc_testgr.py', 'iwc_scripts/iwc_bw_li_inj.py',
              'iwc_scripts/iwc_catalog.py','iwc_scripts/iwc_construct_webpage.py', 'iwc_scripts/iwc_residuals_local_plots.py', 'iwc_pipe/iwc_pipe'],
    
    package_data = {"":["*.css"]},
    classifiers=[
        'Development Status :: 4 - Alpha',
        # Chose either "3 - Alpha", "4 - Beta" or "5 - Production/Stable" as the current state of your package
        'Intended Audience :: Researchers',  # Define that your audience are developers
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: GNU Lesser General Public License v2 (LGPLv2)',  # Again, pick a license
        'Programming Language :: Python :: 3.10',  # Specify which pyhton versions that you want to support
    ],
)
