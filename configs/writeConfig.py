import os
import configparser

# Get the value of the environment variable
iwc_repoDir = os.path.join(os.getenv('IWC_REPO'))

# If the environment variable is not set, prompt the user to enter it
if iwc_repoDir is None:
    iwc_repoDir = input('Please run source {igwn-wave-compare-repoPath}/o4a-sources.sh and try again to write development config file.')

temp_cf_path = os.path.join(iwc_repoDir, 'configs', 'iwc_config_dev.ini')
# Read the existing INI file
config = configparser.ConfigParser()
config.read(temp_cf_path)

# Set the value of the config option using the environment variable
config.set('engine', 'src_path', str(os.path.join(iwc_repoDir, 'iwc_scripts')))

# Save the changes to the INI file
with open(temp_cf_path, 'w') as configfile:
    config.write(configfile)

print('Config succesfully updated with your igwn wave compare repo.')
