import xml.etree.ElementTree as ETwrite
import lxml.etree as ETread

import xml.dom.minidom

from ligo.lw import ligolw, lsctables
from ligo.lw import utils as ligolw_utils

import numpy as np
import pandas as pd 
import os 

from igwn_wave_compare.posterior import extract_maxl_sample, extract_map_sample

'''adjust structure to do IO ops. in parent classes (general naming of {tableName}_columns), data in child classes
(for other analyses)
'''

@lsctables.use_in 
class ligolwcontenthandler(ligolw.LIGOLWContentHandler):
	pass ### just gives us the content handler back
    
'''
MARK DEV: 

- Note to self: parse .h5 files (num. rel.) - get from Meg
- approximant: some NR thing 
- NR data goes in there to accompany
'''
class SimInspiralIO():
    '''MARK DEV: change to be able to create sim_inspiral object without saving file... 
    and probably do away with looping SimInspiralTable object. better to parse structured numpy array or df.
    '''
    '''class for writing constructed data, in numpy array, to SimInspiralTable. '''
    sim_inspiral_out_columns = [
        ('process_id', 'int_8s'),
        ('waveform', 'lstring'),
        ('geocent_end_time', 'int_4s'),
        ('geocent_end_time_ns', 'int_4s'),
        ('h_end_time', 'int_4s'),
        ('h_end_time_ns', 'int_4s'),
        ('l_end_time', 'int_4s'),
        ('l_end_time_ns', 'int_4s'),
        ('g_end_time', 'int_4s'),
        ('g_end_time_ns', 'int_4s'),
        ('t_end_time', 'int_4s'),
        ('t_end_time_ns', 'int_4s'),
        ('v_end_time', 'int_4s'),
        ('v_end_time_ns', 'int_4s'),
        ('end_time_gmst', 'real_8'),
        ('source', 'lstring'),
        ('mass1', 'real_4'),
        ('mass2', 'real_4'),
        ('mchirp', 'real_4'),
        ('eta', 'real_4'),
        ('distance', 'real_4'),
        ('longitude', 'real_4'),
        ('latitude', 'real_4'),
        ('inclination', 'real_4'),
        ('coa_phase', 'real_4'),
        ('polarization', 'real_4'),
        ('psi0', 'real_4'),
        ('psi3', 'real_4'),
        ('alpha', 'real_4'),
        ('alpha1', 'real_4'),
        ('alpha2', 'real_4'),
        ('alpha3', 'real_4'),
        ('alpha4', 'real_4'),
        ('alpha5', 'real_4'),
        ('alpha6', 'real_4'),
        ('beta', 'real_4'),
        ('spin1x', 'real_4'),
        ('spin1y', 'real_4'),
        ('spin1z', 'real_4'),
        ('spin2x', 'real_4'),
        ('spin2y', 'real_4'),
        ('spin2z', 'real_4'),
        ('theta0', 'real_4'),
        ('phi0', 'real_4'),
        ('f_lower', 'real_4'),
        ('f_final', 'real_4'),
        ('eff_dist_h', 'real_4'),
        ('eff_dist_l', 'real_4'),
        ('eff_dist_g', 'real_4'),
        ('eff_dist_t', 'real_4'),
        ('eff_dist_v', 'real_4'),
        ('numrel_mode_min', 'int_4s'),
        ('numrel_mode_max', 'int_4s'),
        ('numrel_data', 'lstring'),
        ('amp_order', 'int_4s'),
        ('taper', 'lstring'),
        ('bandpass', 'int_4s'),
        ('simulation_id', 'int_8s')     
    ]
    
    sim_inspiral_numpy_dt = [
        ('process_id', 'int64'),
        ('waveform', 'U64'),
        ('geocent_end_time', 'int32'),
        ('geocent_end_time_ns', 'int32'),
        ('h_end_time', 'int32'),
        ('h_end_time_ns', 'int32'),
        ('l_end_time', 'int32'),
        ('l_end_time_ns', 'int32'),
        ('g_end_time', 'int32'),
        ('g_end_time_ns', 'int32'),
        ('t_end_time', 'int32'),
        ('t_end_time_ns', 'int32'),
        ('v_end_time', 'int32'),
        ('v_end_time_ns', 'int32'),
        ('end_time_gmst', 'float64'),
        ('source', 'U64'),
        ('mass1', 'float64'),
        ('mass2', 'float64'),
        ('mchirp', 'float64'),
        ('eta', 'float64'),
        ('distance', 'float64'),
        ('longitude', 'float64'),
        ('latitude', 'float64'),
        ('inclination', 'float64'),
        ('coa_phase', 'float64'),
        ('polarization', 'float64'),
        ('psi0', 'float64'),
        ('psi3', 'float64'),
        ('alpha', 'float64'),
        ('alpha1', 'float64'),
        ('alpha2', 'float64'),
        ('alpha3', 'float64'),
        ('alpha4', 'float64'),
        ('alpha5', 'float64'),
        ('alpha6', 'float64'),
        ('beta', 'float64'),
        ('spin1x', 'float64'),
        ('spin1y', 'float64'),
        ('spin1z', 'float64'),
        ('spin2x', 'float64'),
        ('spin2y', 'float64'),
        ('spin2z', 'float64'),
        ('theta0', 'float64'),
        ('phi0', 'float64'),
        ('f_lower', 'float64'),
        ('f_final', 'float64'),
        ('eff_dist_h', 'float64'),
        ('eff_dist_l', 'float64'),
        ('eff_dist_g', 'float64'),
        ('eff_dist_t', 'float64'),
        ('eff_dist_v', 'float64'),
        ('numrel_mode_min', 'int32'),
        ('numrel_mode_max', 'int32'),
        ('numrel_data', 'U64'),
        ('amp_order', 'int32'),
        ('taper', 'U64'),
        ('bandpass', 'int32'),
        ('simulation_id', 'int64')
    ]
    

    def __init__(self):
        pass 
        ### use all class methods, for now (if we parse samples in this class, instantiate holders here)
    
    
    
    @classmethod
    def makeXML(cls, sampleArr, write=None):
        '''write a sim_inspiral table based to outFile path and sampleArr conditioned in igwn_wave_compare/posterior.py
        igwn_wave_compare/posterior.py: migrate some of that to here (makes most sense)
        '''
        xmldoc = ligolw.Document()
        xmldoc.appendChild(ligolw.LIGO_LW())

        processtable = lsctables.New(lsctables.ProcessTable)
        # processparamstable = lsctables.New(lsctables.ProcessParamsTable) ### ditch this, see if works
        siminspiraltable = lsctables.New(lsctables.SimInspiralTable)

        xmldoc.childNodes[-1].appendChild(processtable)
        # xmldoc.childNodes[-1].appendChild(processparamstable) ### no set in stone params? seemed to be able to do without this in GstLal
        xmldoc.childNodes[-1].appendChild(siminspiraltable)
        
        ### loop through sample_array (structured numpy)
        for samp_ind, samp in enumerate(sampleArr):
            ### make blank array
            siminspiraltable.append(siminspiraltable.RowType())
            ### loop through sim_inspiral_columns (class prop)
            ### should have all of these properly stored in sim_inspiral_columns
            
            for ind, (column, col_type) in enumerate(cls.sim_inspiral_out_columns):
                if column in samp.dtype.names:
                    setattr(siminspiraltable[-1], column, samp[column])
                elif col_type == 'lstring':
                    setattr(siminspiraltable[-1], column, "")
                else:
                    setattr(siminspiraltable[-1], column, 0)

        ### if file path specified, check writable, write to file
        if write != None: 
            print('WRITING XML FILE')
            if os.access(os.path.dirname(write), os.W_OK):
                ligolw_utils.write_filename(xmldoc, write, verbose=True)
            else:
                raise IOError('Output path of XML is not writable. Please enter a valid path') ### for general use-cases
        
        return siminspiraltable
            
        
    @classmethod
    def parseXML(cls, filePath):
        ### load xml file
        xmldoc  = ligolw_utils.load_filename(filePath, False, contenthandler=ligolwcontenthandler)
        ### get sim_inspiral_table from inside xmldoc object
        return lsctables.SimInspiralTable.get_table(xmldoc)
        ### TODO: add check for if xmldoc contains SimInspiral or not  
    
class BilbySamplesIO(SimInspiralIO):

    # def __init__(self):
    #     pass
    
    @classmethod
    def samples2table(cls, samples, pe_approx, pe_flow, pe_amporder, injfile_name=None, trigtimes=None, sampleType='draws'):

        '''
        Types of injections:
        (writing to file determined if injfile_name is None or not)
        (passing in an numpy.ndarray will determine how many injections are made (ninj); is only designed to be used without for single injections)
        draws: Posterior draws made ninj times into SimInspiral table.         
        ML: Max (log) likelihood waveform is injected ninj times into SimInspiral table.
        single: Create SimInspiral table of one entry, the max log likelihood WF.
        '''
        if sampleType not in ['draws', 'ML', 'MAP', 'singleLogPost', 'singleLogL']:
            raise ValueError('You have entered an sampleType different from "draws", "ML", "MAP", "singleLogPost", or "singleLogL". Choose one of these and try again')
        
        if isinstance(trigtimes, np.ndarray):
            
            # pe_structured_array = np.genfromtxt(pe_file, names=True) ##
            post_samples_init = pd.DataFrame(samples) ##
            cols = post_samples_init.columns
            
            if sampleType in ['ML', 'MAP', 'draws']:
                ninj = trigtimes.shape[0] ### if only have one sample passed in and trigtimes array, should function same as single options 
            else: ninj = 1 ### only one injection

            if sampleType == 'ML':
                maxLRow = extract_maxl_sample(samples)
                post_samples = pd.DataFrame(np.repeat(maxLRow, ninj, axis=0), columns=cols)
            elif sampleType == 'draws':
                post_samples = post_samples_init.sample(n=ninj)
            elif sampleType == 'MAP':
                maxMAPRow = extract_map_sample(samples)
                post_samples = pd.DataFrame(np.repeat(maxMAPRow, ninj, axis=0), columns=cols)
            elif sampleType == 'singleLogL':
                post_samples = extract_maxl_sample(post_samples_init) ### and do not duplicate! Single trigtime.
            elif sampleType == 'singleLogPost': ### only one injection
                post_samples = extract_map_sample(post_samples_init)
            
            if len(trigtimes) != 1 and sampleType in ['singleLogPost', 'singleLogL']:
                raise ValueError('You have opted for "singleLogL" or "singleLogPost" option which makes a single injection. Please use a trigtimes array of only one entry (a single trigtime)')
            
            injections = np.zeros((ninj), dtype=cls.sim_inspiral_numpy_dt) ##

            ### we're going to overwrite the geocenter time params from the PE sample(s) with the trigtimes we have selected
            trigtimes_ns, trigtimes_s = np.modf(trigtimes)
            trigtimes_ns *= 10 ** 9

        elif trigtimes is None:

            ### pe_structured_array = np.genfromtxt(pe_file, names=True) ##
            post_samples_init = pd.DataFrame(samples) ### changed to accept samples in call of this func.
            
            if sampleType in ['ML', 'MAP', 'draws']:
                ninj = len(samples) ### if only have one sample passed in and no trigtimes, should function same as single options. Otherwise, same trigtime in SimInspiral table (?)
            else: ninj = 1 ### only one injection
                
            if sampleType == 'ML':
                maxLVals_df = extract_maxl_sample(samples)
                post_samples = pd.DataFrame(np.repeat(maxLVals_df.values, ninj, axis=0))
            elif sampleType == 'draws':
                ## number of samples equal to number of PE samples, in this case
                post_samples = post_samples_init
            elif sampleType == 'MAP':
                maxMAPVals_df = extract_map_sample(samples)
                post_samples = pd.DataFrame(np.repeat(maxMAPVals_df.values, ninj, axis=0))   
            elif sampleType == 'singleLogL':
                post_samples = extract_maxl_sample(samples)
            elif sampleType == 'singleLogPost':
                post_samples = extract_map_sample(samples)
                
            injections = np.zeros((ninj), dtype=cls.sim_inspiral_numpy_dt)

            ### use trigtimes from samples file if we don't have trigtimes (as in old implementation), fill in everything else from post_samples (maxL values)
            ### for residuals/waveform consistency analysis: this should be for a single sample which we make into a simInspiral just for convenience
            try:
                trigtimes_ns, trigtimes_s = np.modf(post_samples["geocent_time"])
            except KeyError:
                trigtimes_ns, trigtimes_s = np.modf(post_samples["time"])
            trigtimes_ns *= 10 ** 9
        else:
            raise ValueError('Please enter either a valid PE samples file or a numppy.ndarray of trigtimes.')
        
        injections["geocent_end_time"] = trigtimes_s
        injections["geocent_end_time_ns"] = trigtimes_ns

        injections["waveform"] = [pe_approx for _ in range(ninj)]
        injections["taper"] = ["TAPER_NONE" for _ in range(ninj)]
        injections["f_lower"] = [pe_flow for _ in range(ninj)]
        try:
            injections["mchirp"] = np.array(post_samples["chirp_mass"])
        except KeyError:
            injections["mchirp"] = np.array(post_samples["mc"])
        try:
            injections["eta"] = np.array(post_samples["symmetric_mass_ratio"])
        except KeyError:
            injections["eta"] = np.array(post_samples["eta"])
        try:
            injections["mass1"] = np.array(post_samples["mass_1"])
        except KeyError:
            injections["mass1"] = np.array(post_samples["m1"])
        try:
            injections["mass2"] = np.array(post_samples["mass_2"])
        except KeyError:
            injections["mass2"] = np.array(post_samples["m2"])

        try:
            injections["distance"] = np.array(post_samples["luminosity_distance"])
        except KeyError:
            injections["distance"] = np.array(post_samples["dist"])
        injections["longitude"] = np.array(post_samples["ra"])
        injections["latitude"] = np.array(post_samples["dec"])
        injections["inclination"] = np.array(post_samples["iota"])
        injections["coa_phase"] = np.array(post_samples["phase"])
        injections["polarization"] = np.array(post_samples["psi"])
        try:
            injections["spin1x"] = np.array(post_samples["spin_1x"])
        except KeyError:
            injections["spin1x"] = np.array(post_samples["a1"]) * np.sin(np.array(post_samples["theta1"])) * np.cos(
                np.array(post_samples["phi1"]))
        try:
            injections["spin1y"] = np.array(post_samples["spin_1y"])
        except KeyError:
            injections["spin1y"] = np.array(post_samples["a1"]) * np.sin(np.array(post_samples["theta1"])) * np.sin(
                np.array(post_samples["phi1"]))
        try:
            injections["spin1z"] = np.array(post_samples["spin_1z"])
        except KeyError:
            injections["spin1z"] = np.array(post_samples["a1"]) * np.cos(np.array(post_samples["theta1"]))
        try:
            injections["spin2x"] = np.array(post_samples["spin_2x"])
        except KeyError:
            injections["spin2x"] = np.array(post_samples["a2"]) * np.sin(np.array(post_samples["theta2"])) * np.cos(
                np.array(post_samples["phi2"]))
        try:
            injections["spin2y"] = np.array(post_samples["spin_2y"])
        except KeyError:
            injections["spin2y"] = np.array(post_samples["a2"]) * np.sin(np.array(post_samples["theta2"])) * np.sin(
                np.array(post_samples["phi2"]))
        try:
            injections["spin2z"] = np.array(post_samples["spin_2z"])
        except KeyError:
            injections["spin2z"] = np.array(post_samples["a2"]) * np.cos(np.array(post_samples["theta2"]))
        injections["amp_order"] = [pe_amporder for _ in range(ninj)]
        injections["numrel_data"] = ["" for _ in range(ninj)]

        ### updated for new LIGOLW XML file format
        inds = np.array(list(range(ninj)))
        zeros = np.zeros([ninj])
        injections["process_id"] = zeros
        injections["simulation_id"] = inds ### i.e. event number

        ### fill in unused cols with 0's
        for column, dtype in cls.sim_inspiral_numpy_dt:
            if column not in injections.dtype.names:
                injections[column] = np.zeros(ninj, dtype=dtype)
                
                
        if injfile_name is None:
            return cls.makeXML(injections, write=None)
        else: 
            return cls.makeXML(injections, write=injfile_name)
            
            
class RiftSamplesIO(SimInspiralIO):
    def __init__(self):
        pass
    
    @classmethod
    def parse_samples_table(cls, pe_file, pe_approx, pe_flow, pe_amporder, injfile_name, trigtimes=None):
        pass ### dev in progress
    
    
class NRParserIO(SimInspiralIO):
    def __init__(self):
        pass