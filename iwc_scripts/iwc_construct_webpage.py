#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2021  Megan Arogeti <megan.arogeti@ligo.org>
#           (C) 2021 John Michael Sullivan <johnmichael.sullivan@ligo.org>
#           (C) 2021  Sudarshan Ghonge <sudarshan.ghonge@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from iwc_web_utils import htmlGen as HTML
import os
import sys
import argparse
import importlib_resources
from pathlib import Path



"""
HTML Generation script by Jack Sullivan for LSC resesarch. 

Currently: in testing mode to test HTML output. In process of adding file pathing, tables, and additional info.

(all condiitonal logic commented out)

"""

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("--onsource-recon-path", default=None, type=str,
                    help="onsource reconstructions directory path.")
parser.add_argument("--sim-onsource-res", default=False, action="store_true",
                    help="Report on simulated onsource residuals run.")
parser.add_argument("--onsource-res-path", default=None, type=str,
                    help="onsource residuals directory path.")
# it didnt like offsource-injections in Python
parser.add_argument("--offsource-injections-path", default=None, type=str,
                    help="offsource injections directory path.")
parser.add_argument("--sim-offsource-injections-path", default=None, type=str,
                    help="offsource injections directory path.")
parser.add_argument("--offsource-background-path", default=None, type=str,
                    help="offsource background runs directory path.")
# parser.add_argument("--sim-offsource-background-path", default=None, type=str,
#                     help="offsource background runs directory path.")

parser.add_argument("--ifos", default=None, nargs='+', choices=['H1', 'L1', 'V1'])
### testgr-dir and catalog-dir not passed by default; default values are where things should be
parser.add_argument("--testgr-dir", default='testgr/testgr_output', type=str,
                    help="Directory for TGR results.")
parser.add_argument("--catalog-dir", default='catalog/catalog_output', type=str,
                    help="Directory for WF consistency test (catalog.py) results.")

parser.add_argument("--event-name", default='', type=str,
                    help="Name of rundir.")

parser.add_argument("--topdir", default=None, type=str,
                    help="Top directory address of run.")
opts = parser.parse_args()

ref = importlib_resources.files('iwc_web_utils')
postprocesspath = str(ref)
os.system('cp ' + postprocesspath + '/master.css ' + '.')

f = open('iwc_output.html', 'w')

bigS = ""

if opts.event_name is not None:
    bigS += HTML.head(opts.event_name)
else:
    print('Error: Event name for display not passed. Check opts and try again.')
    sys.exit(1)


navDivString = ""

navContentString = ""

# ---------------------------------------
# BEGIN CONDITIONAL LOGIC

# ONSOURCE RECONSTRUCTION SECTION
if opts.onsource_recon_path is not None:
    onsourceReconString = ""
    onsourceReconString += HTML.h1("Onsource Reconstruction", False)
    onsourceReconString += HTML.h2("Reconstruction Comparison", False)
    # alter specifics here
    # relOnsPath = '/'.join(opts.onsource_recon_path.split('/')[-2:])
    imagepath = os.path.join(opts.onsource_recon_path, "LI_reconstruct", "reconstruction_comparison.png")
    onsourceReconString += HTML.img(imagepath)
    
    # table here
    ### MARK DEV: file transfer

    absOnsPath = os.path.join(opts.topdir, 'onsource_reconstruction', os.path.basename(opts.onsource_recon_path))
    tableAddr = os.path.join(absOnsPath, "LI_reconstruct", "all_stats_overlaps.md")
    
    onsourceReconString += HTML.h2("Overlaps", True) + HTML.table(tableAddr)

    # now package this in div: 
    navDivString += HTML.sectiondiv("Onsource Reconstruction", onsourceReconString)

    navContentString += HTML.navItem("Onsource Reconstruction", True)

# ONSOURCE RESIDUALS SECTION
if opts.onsource_res_path is not None and not opts.sim_onsource_res:
    onsourceResString = ""
    onsourceResString += HTML.h1("Onsource Residuals", False)
    onsourceResString += HTML.h2("Whitened Data", False)
    # something here! 

    imagepath = os.path.join(os.path.dirname(opts.onsource_res_path), "residual_frame", "white_data_res.png")
    onsourceResString += HTML.img(imagepath)

    onsourceResString += HTML.h2("Coherent Residuals Reconstructions", False)

    # imagepath = glob.glob('test_assets/snr_residuals.png')[0]
    imagepaths = [os.path.join(opts.onsource_res_path, "plots", "signal_waveform_{}.png".format(ifo))
                  for ifo in opts.ifos]

    onsourceResString += HTML.imgGridVar(imagepaths)

    # TESTING NEW GRID COMPONENT; COMMENTING BELOW OUT
    # for imagepath in imagepaths:
    #     onsourceResString += HTML.img(imagepath)

    # Combining:
    navDivString += HTML.sectiondiv("Onsource Residuals", onsourceResString)
    navContentString += HTML.navItem("Onsource Residuals", False)  # not the default active in the menu anymore
    
if opts.onsource_recon_path is not None and opts.offsource_injections_path is not None:
    reconAnalysisStr = ""
    reconAnalysisStr += HTML.h1("Waveform Consistency Analysis:\nReal Offsource Data", False)
    reconAnalysisStr += HTML.h2("Catalog Comparison", False)
    ### mark dev
    imagepath = f"{opts.catalog_dir}/catalog_comparison.png"
        # store button location in variable here! ->
    buttonAddress = f"{opts.catalog_dir}/overlaps.dat"
    reconAnalysisStr += HTML.linkButton(buttonAddress)
    
    reconAnalysisStr += HTML.img(imagepath)
    navDivString += HTML.sectiondiv("Reconstruction Analysis", reconAnalysisStr)
    navContentString += HTML.navItem("Reconstruction Analysis", False)
    
if opts.onsource_recon_path is not None and opts.sim_offsource_injections_path is not None:
    reconAnalysisStr = ""
    reconAnalysisStr += HTML.h1("Waveform Consistency Analysis:\nReal Onsource Data | Simulated Offsource Data", False)
    reconAnalysisStr += HTML.h2("Catalog Comparison", False)
    imagepath = f"{opts.catalog_dir}/catalog_comparison.png"
    reconAnalysisStr += HTML.img(imagepath)
    buttonAddress = f"{opts.catalog_dir}/overlaps.dat"
    reconAnalysisStr += HTML.linkButton(buttonAddress)
    
    navDivString += HTML.sectiondiv("Reconstruction Analysis", reconAnalysisStr)
    navContentString += HTML.navItem("Reconstruction Analysis", False)

if opts.onsource_res_path is not None and opts.offsource_background_path is not None:
    resAnalysisStr = ""
    resAnalysisStr += HTML.h1("Residuals Analysis:\nReal Onsource Data | Real Offsource Data", False)
    resAnalysisStr += HTML.h2("SNR Residuals", False)
    imagepath = f"{opts.testgr_dir}/snr_residuals.png"
    resAnalysisStr += HTML.img(imagepath)
        # # store download button location in variable here! ->
    buttonAddress = f"{opts.testgr_dir}/offsource_snr_90s.dat"
    resAnalysisStr += HTML.linkButton(buttonAddress)

    navDivString += HTML.sectiondiv("Residuals Analysis", resAnalysisStr)
    navContentString += HTML.navItem("Residuals Analysis", False)

if opts.onsource_res_path is not None and opts.sim_onsource_res:
    resAnalysisStr = ""
    resAnalysisStr += HTML.h1("Simulated Onsource Residual SNR & Fitting Factor\nDistributions", False)
    # resAnalysisStr += HTML.h2("Onsource Residual SNR & Fitting Factor\nDistributions", False)
    imagepath = f"{opts.testgr_dir}/residual-snr_FF-distributions.png"
    resAnalysisStr += HTML.img(imagepath)
        # # store download button location in variable here! ->
    # buttonAddress = f"{opts.testgr_dir}/offsource_snr_90s.dat"
    # resAnalysisStr += HTML.linkButton(buttonAddress)

    navDivString += HTML.sectiondiv("Simulated Residuals Analysis", resAnalysisStr)
    navContentString += HTML.navItem("Simulated Residuals Analysis", False)

# HTML.navBones(navContentString)
bigS += (HTML.navBones(navContentString, opts.event_name)) + HTML.container("bodContent", navDivString)
bigS += HTML.footer()

f.write(bigS)
f.close()
